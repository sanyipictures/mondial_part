﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hazi3
{
    class Sea
    {
        public String ID { get; set; }
        public String Name { get; set; }
        public int Depth { get; set; }
        public List<Located> L_ocated { get; set; }
    }
}
