﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hazi3
{
    class Mondial
    {
        public List<Continent> Continents { get; set; }
        public List<Country> Countries { get; set; }
        public List<Organization> Organizations { get; set; }
        public List<Mountain> Mountains { get; set; }
        public List<Desert> Deserts { get; set; }
        public List<Island> Islands { get; set; }
        public List<River> Rivers { get; set; }
        public List<Sea> Seas { get; set; }
        public List<Lake> Lakes { get; set; }
    }
}
