﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hazi3
{
    class City
    {
        public String ID { get; set; }
        public String Country_ { get; set; }
        public String Province_ { get; set; }
        public String Longitude { get; set; }
        public String Latitude { get; set; }
        public String Name { get; set; }
        public LocatedAt LocatedAt_ { get; set; }
        public Population Population_ { get; set; }
    }
}
