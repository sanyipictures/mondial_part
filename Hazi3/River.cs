﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hazi3
{
    class River
    {
        public String ID { get; set; }
        public int Length { get; set; }
        public String Name { get; set; }
        public To To_ { get; set; }
        public List<Located> L_ocated { get; set; }
    }
}
