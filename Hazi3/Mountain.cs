﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hazi3
{
    class Mountain
    {
        public String ID { get; set; }
        public String Name { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public int Height { get; set; }
        public List<Located> L_ocated { get; set; }
    }
}
