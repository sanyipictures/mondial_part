﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hazi3
{
    class Island
    {
        public String ID { get; set; }
        public String Name { get; set; }
        public double Area { get; set; }
        public List<Located> L_ocated { get; set; }
    }
}
