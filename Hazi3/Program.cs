﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Hazi3
{
    class Temp {
        public String OceanID { get; set; }
        public int Count { get; set; }
    }
    class EthniTemp {
        public String EthName{ get; set; }
        public int Amount { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {
            String path = "http://www.szabozs.hu/_DEBRECEN/kerteszg/mondial-3.0.xml";

            XDocument mondialXML = XDocument.Load(path);

            var continents = from x in mondialXML.Descendants("continent")
                             select new Continent
                             {
                                 ID = x.Attribute("id")?.Value,
                                 Name = x.Attribute("name")?.Value
                             };

            var countrys = from x in mondialXML.Descendants("country")
                           select new Country()
                           {
                               ID = x.Attribute("id")?.Value ?? "notSpecified",
                               Name = x.Attribute("name")?.Value ?? "notSpecified",
                               Capital = x.Attribute("capital")?.Value ?? "notSpecified",
                               Population = (int?)int.Parse(x.Attribute("population")?.Value) ?? 0,
                               Datacode = x.Attribute("datacode")?.Value ?? "notSpecified",
                               TotalArea = (int?)int.Parse(x.Attribute("total_area")?.Value) ?? 0,
                               PopulationGrowth = x.Attribute("population_growth") != null ? double.Parse(x.Attribute("population_growth").Value.Replace(".", ",")) : 0,
                               InfantMortality = x.Attribute("infant_mortality") != null ? double.Parse(x.Attribute("infant_mortality").Value.Replace(".", ",")) : 0,
                               GDPAgri = x.Attribute("gdp_agri") != null ? double.Parse(x.Attribute("gdp_agri").Value.Replace(".", ",")) : 0,
                               GDPInd = x.Attribute("gdp_ind") != null ? double.Parse(x.Attribute("gdp_ind").Value.Replace(".", ",")) : 0,
                               GDPServ = x.Attribute("gdp_serv") != null ? double.Parse(x.Attribute("gdp_serv").Value.Replace(".", ",")) : 0,
                               GDPTotal = x.Attribute("gdp_total") != null ? double.Parse(x.Attribute("gdp_total").Value.Replace(".", ",")) : 0,
                               Inflation = x.Attribute("inflation") != null ? double.Parse(x.Attribute("inflation").Value.Replace(".", ",")) : 0,
                               IndepDate = x.Attribute("indep_date")?.Value ?? "notSpecified",
                               Goverment = x.Attribute("government")?.Value ?? "notSpecified",
                               CarCode = x.Attribute("car_code")?.Value ?? "notSpecified",
                               Cities = x.Descendants("city").Select(a => new City()
                               {
                                   ID = a.Attribute("id")?.Value ?? "notSpecified",
                                   Country_ = a.Attribute("country")?.Value ?? "notSpecified",
                                   Province_ = a.Attribute("province")?.Value ?? "notSpecified",
                                   Longitude = a.Attribute("longitude")?.Value ?? "notSpecified",
                                   Latitude = a.Attribute("latitude")?.Value ?? "notSpecified",
                                   Name = a.Element("name")?.Value ?? "notSpecified",
                                   LocatedAt_ = new LocatedAt()
                                   {
                                       Type = a.Element("located_at")?.Attribute("type")?.Value ?? "notSpecified",
                                       Water = a.Element("located_at")?.Attribute("water")?.Value ?? "notSpecified"
                                   },
                                   Population_ = new Population()
                                   {
                                       Year = a.Element("population") != null ? int.Parse(a.Element("population").Attribute("year")?.Value) : 0,
                                       Population_ = a.Element("population") != null ? int.Parse(a.Element("population")?.Value) : 0
                                   }
                               }).ToList(),
                               Ethnicgroups_ = x.Descendants("ethnicgroups").Select(b => new Ethnicgroups()
                               {
                                   Percentage = b.Attribute("percentage") != null ? double.Parse(b.Attribute("percentage").Value.Replace(".", ",")) : 0,
                                   EthName = b?.Value ?? "notSpecified"
                               }).ToList(),
                               Religions_ = x.Descendants("religions").Select(c => new Religions()
                               {
                                   Percentage = c.Attribute("percentage") != null ? double.Parse(c.Attribute("percentage")?.Value.Replace(".", ",")) : 0,
                                   RelName = c?.Value ?? "notSpecified"
                               }).ToList(),
                               Encompassed_ = new Encompassed
                               {
                                   Continent_ = x.Element("encompassed")?.Attribute("continent")?.Value ?? "notSpecified",
                                   Percentage = (int?)int.Parse(x.Element("encompassed").Attribute("percentage")?.Value) ?? 0
                               },
                               Borders = x.Descendants("border").Select(d => new Border()
                               {
                                   Length = d.Attribute("length") != null ? double.Parse(d.Attribute("length")?.Value.Replace(".", ",")) : 0,
                                   Country_ = d.Attribute("country")?.Value ?? "notSpecified"
                               }).ToList(),
                               Provinces = x.Descendants("province").Select(e => new Province()
                               {
                                   ID = e.Attribute("id")?.Value ?? "notSpecified",
                                   Name = e.Attribute("name")?.Value ?? "notSpecified",
                                   Country = e.Attribute("country")?.Value ?? "notSpecified",
                                   Capital = e.Attribute("capital")?.Value ?? "notSpecified",
                                   Population_ = e.Attribute("population") != null ? int.Parse(e.Attribute("population").Value) : 0,
                                   Area = e.Attribute("area") != null ? int.Parse(e.Attribute("area").Value) : 0,
                                   Cities = e.Descendants("city").Select(ex => new City()
                                   {
                                       ID = ex.Attribute("id")?.Value ?? "notSpecified",
                                       Country_ = ex.Attribute("country")?.Value ?? "notSpecified",
                                       Province_ = ex.Attribute("province")?.Value ?? "notSpecified",
                                       Longitude = ex.Attribute("longitude")?.Value ?? "notSpecified",
                                       Latitude = ex.Attribute("latitude")?.Value ?? "notSpecified",
                                       Name = ex.Element("name")?.Value ?? "notSpecified",
                                       LocatedAt_ = new LocatedAt()
                                       {
                                           Type = ex.Element("located_at")?.Attribute("type")?.Value ?? "notSpecified",
                                           Water = ex.Element("located_at")?.Attribute("water")?.Value ?? "notSpecified"
                                       },
                                       Population_ = new Population()
                                       {
                                           Year = ex.Element("population") != null ? int.Parse(ex.Element("population").Attribute("year").Value) : 0,
                                           Population_ = ex.Element("population") != null ? int.Parse(ex.Element("population").Value) : 0
                                       }
                                   }).ToList()
                               }).ToList(),
                               Languages_ = x.Descendants("languages").Select(f => new Languages()
                               {
                                   Percentage = f.Attribute("percentage") != null ? double.Parse(f.Attribute("percentage")?.Value.Replace(".", ",")) : 0,
                                   NameOfLanguage = f?.Value ?? "notSpecified"
                               }).ToList()
                           };

            var organizations = from x in mondialXML.Descendants("organization")
                                select new Organization
                                {
                                    ID = x.Attribute("id")?.Value ?? "notSpecified",
                                    Name = x.Attribute("name")?.Value ?? "notSpecified",
                                    Abbrev = x.Attribute("abbrev")?.Value ?? "notSpecified",
                                    Established = x.Attribute("established")?.Value ?? "notSpecified",
                                    Headq = x.Attribute("headq")?.Value ?? "notSpecified",
                                    M_embers = x.Descendants("members").Select(y => new Members
                                    {
                                        Type = y.Attribute("type")?.Value ?? "notSpecified",
                                        Country = y.Attribute("country")?.Value ?? "notSpecified"
                                    }).ToList()
                                };

            var mountains = from x in mondialXML.Descendants("mountain")
                            select new Mountain()
                            {
                                ID = x.Attribute("id")?.Value ?? "notSpecified",
                                Name = x.Attribute("name")?.Value ?? "notSpecified",
                                Longitude = x.Attribute("longitude") != null ? double.Parse(x.Attribute("longitude").Value.Replace(".", ",")) : 0,
                                Latitude = x.Attribute("latitude") != null ? double.Parse(x.Attribute("latitude").Value.Replace(".", ",")) : 0,
                                Height = x.Attribute("height") != null ? int.Parse(x.Attribute("height").Value) : 0,
                                L_ocated = x.Descendants("located").Select(y => new Located()
                                {
                                    Country = y.Attribute("country")?.Value ?? "notSpecified",
                                    Province = y.Attribute("province")?.Value ?? "notSpecified"
                                }).ToList()
                            };

            var deserts = from x in mondialXML.Descendants("desert")
                          select new Desert()
                          {
                              ID = x.Attribute("id")?.Value ?? "notSpecified",
                              Name = x.Attribute("name")?.Value ?? "notSpecified",
                              Area = x.Attribute("area") != null ? int.Parse(x.Attribute("area").Value) : 0,
                              L_ocated = x.Descendants("located").Select(y => new Located()
                              {
                                  Country = y.Attribute("country")?.Value ?? "notSpecified",
                                  Province = y.Attribute("province")?.Value ?? "notSpecified"
                              }).ToList()
                          };

            var islands = from x in mondialXML.Descendants("island")
                          select new Island()
                          {
                              ID = x.Attribute("id")?.Value ?? "notSpecified",
                              Name = x.Attribute("name")?.Value ?? "notSpecified",
                              Area = x.Attribute("area") != null ? double.Parse(x.Attribute("area").Value.Replace(".", ",")) : 0,
                              L_ocated = x.Descendants("located").Select(y => new Located()
                              {
                                  Country = y.Attribute("country")?.Value ?? "notSpecified",
                                  Province = y.Attribute("province")?.Value ?? "notSpecified"
                              }).ToList()
                          };

            var rivers = from x in mondialXML.Descendants("river")
                         select new River()
                         {
                             ID = x.Attribute("id")?.Value ?? "notSpecified",
                             Name = x.Attribute("name")?.Value ?? "notSpecified",
                             Length = x.Attribute("length") != null ? int.Parse(x.Attribute("length").Value) : 0,
                             To_ = new To()
                             {
                                 Type = x.Element("to")?.Attribute("type")?.Value ?? "notSpecified",
                                 Water = x.Element("to")?.Attribute("water")?.Value ?? "notSpecified"
                             },
                             L_ocated = x.Descendants("located").Select(y => new Located()
                             {
                                 Country = y.Attribute("country")?.Value ?? "notSpecified",
                                 Province = y.Attribute("province")?.Value ?? "notSpecified"
                             }).ToList()
                         };

            var seas = from x in mondialXML.Descendants("sea")
                       select new Sea()
                       {
                           ID = x.Attribute("id")?.Value ?? "notSpecified",
                           Name = x.Attribute("name")?.Value ?? "notSpecified",
                           Depth = x.Attribute("depth") != null ? int.Parse(x.Attribute("depth").Value) : 0,
                           L_ocated = x.Descendants("located").Select(y => new Located()
                           {
                               Country = y.Attribute("country")?.Value ?? "notSpecified",
                               Province = y.Attribute("province")?.Value ?? "notSpecified"
                           }).ToList()
                       };

            var lakes = from x in mondialXML.Descendants("lake")
                        select new Lake()
                        {
                            ID = x.Attribute("id")?.Value ?? "notSpecified",
                            Name = x.Attribute("name")?.Value ?? "notSpecified",
                            Area = x.Attribute("area") != null ? double.Parse(x.Attribute("area").Value.Replace(".", ",")) : 0,
                            L_ocated = x.Descendants("located").Select(y => new Located()
                            {
                                Country = y.Attribute("country")?.Value ?? "notSpecified",
                                Province = y.Attribute("province")?.Value ?? "notSpecified"
                            }).ToList()
                        };
            Mondial mondialInstance = new Mondial();
            mondialInstance.Continents = continents.ToList();

            mondialInstance.Organizations = organizations.ToList();

            mondialInstance.Countries = countrys.ToList();

            mondialInstance.Mountains = mountains.ToList();

            mondialInstance.Deserts = deserts.ToList();

            mondialInstance.Islands = islands.ToList();

            mondialInstance.Rivers = rivers.ToList();

            mondialInstance.Seas = seas.ToList();

            mondialInstance.Lakes = lakes.ToList();

            ////melyik ország népsürűsége a legnagyobb?
            //Country asd = mondialInstance.Countries.OrderByDescending(x => x.Population / x.TotalArea).First();
            //Console.WriteLine("Name: {0}, Area: {1}, Population: {2}, Suruseg: {3}", asd.Name, asd.TotalArea, asd.Population, asd.Population / asd.TotalArea);
            //Console.WriteLine("/////////////////////////////////////////////////////");

            ////Az országok hány százalékában van valamilyen formájú demokrácia
            //Console.WriteLine(
            //        (int)
            //            (
            //                (
            //                (double)mondialInstance.Countries.Where(x => x.Goverment.Contains("democracy")).Count()
            //                /
            //                (double)(mondialInstance.Countries.Count())
            //                )
            //                * 100
            //            )
            //    + "%");

            ////Vallási és etnikai csoportok száma országonként, összegük szerinti csökkenő sorrendben
            //var q1 = from x in mondialInstance.Countries
            //         select new
            //         {
            //             Summ = x.Religions_.Count + x.Ethnicgroups_.Count,
            //             Name = x.Name
            //         };
            //foreach (var item in q1.OrderBy(x => x.Summ).Reverse())
            //{
            //    Console.WriteLine("Country name: {0}, and sum of religions and ethnigroups: {1}", item.Name, item.Summ);
            //}

            ////Határok hossza országonként
            //var q2 = from x in mondialInstance.Countries
            //         select new
            //         {
            //             Summ = x.Borders.Select(y => y.Length).Sum(),
            //             Name = x.Name
            //         };
            //foreach (var item in q2)
            //{
            //    Console.WriteLine("Country name: \t{0}, BorderLength: \t{1}", item.Name, item.Summ);
            //}

            ////A legtöbb országot metsző 5 folyó
            //var q3 = from x in mondialInstance.Rivers
            //         where x.L_ocated.Count >= 2
            //         select new
            //         {
            //             Name = x.Name,
            //             Cnt = x.L_ocated.Count
            //         };
            //foreach (var item in q3.OrderBy(x => x.Cnt).Reverse().Take(5))
            //{
            //    Console.WriteLine("RiverName: {0}, \t CountriesCuted: {1}", item.Name, item.Cnt);
            //}

            ////Azon 5 ország ahol a legnagyobb számban találunk folyót, tavat és tengert összesen
            //var q4 = from x in mondialXML.Descendants("country")
            //         group x by x.Attribute("name").Value into asd
            //         select new
            //         {
            //             Name = asd.Key,
            //             Amount = asd.Descendants("located_at").Attributes("water").Select(y => y.Value).Distinct().Count()
            //         };

            //foreach (var item in q4.OrderByDescending(x => x.Amount).Take(5))
            //{
            //    Console.WriteLine(item);
            //}

            ////Mely tengerekbe folyik a legtöbb folyó
            //var q1 = mondialXML.Descendants("river")
            //    .Where(x => x.Element("to").Attribute("type").Value.Contains("sea"))
            //    .GroupBy(x => x.Element("to").Attribute("water").Value)
            //    .Select(x => new Temp()
            //    {
            //        OceanID = x.Key,
            //        Count = x.Count()
            //    });
            //var q2 = mondialInstance.Seas
            //    .Select(x => new {
            //        id = x.ID,
            //        name = x.Name
            //    });
            //List<Temp> neves = new List<Temp>();

            //foreach (var item in q2)
            //{
            //    foreach (var item2 in q1)
            //    {
            //        if (item2.OceanID.Equals(item.id)) {
            //            neves.Add(new Temp() { Count = item2.Count, OceanID = item.name });
            //        }
            //    }
            //}

            //foreach (var item in neves.OrderByDescending(x => x.Count))
            //{
            //    Console.WriteLine("ID: {0}, Count: {1}",item.OceanID, item.Count);
            //}

            //A világ etnikumainak mérete(ország lakossága *etnikai csoport aránya)
            //var etnigroupTypes = mondialInstance.Countries.SelectMany(x => x.Ethnicgroups_).Select(y => y.EthName).Distinct();
            //var ethnigroups = mondialXML.Descendants("ethnicgroups");

            //List<EthniTemp> ethnytemp = new List<EthniTemp>();
            //double sum = 0;
            //foreach (var item in etnigroupTypes)
            //{
            //    foreach (var item2 in ethnigroups)
            //    {
            //        if (item2.Value.Equals(item)) {
            //            sum += double.Parse(item2.Attribute("percentage").Value.Replace(".", ",")) * int.Parse(item2.Parent.Attribute("population").Value);
            //        }
            //    }
            //    sum /= 100;
            //    ethnytemp.Add(new EthniTemp() { EthName = item, Amount = (int)sum });
            //    sum = 0;
            //}

            //foreach (var item in ethnytemp.OrderByDescending(x => x.Amount))
            //{
            //    Console.WriteLine("Eth_name: {0}, population: {1}", item.EthName, item.Amount);
            //}

            Console.WriteLine("done");
            Console.ReadKey();
        }
    }
}

