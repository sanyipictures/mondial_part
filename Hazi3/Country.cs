﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hazi3
{
    class Country
    {
        public String ID { get; set; }
        public String Name { get; set; }
        public String Capital { get; set; }
        public int Population { get; set; }
        public String Datacode { get; set; }
        public int TotalArea { get; set; }
        public double PopulationGrowth { get; set; }
        public double InfantMortality { get; set; }
        public double GDPAgri { get; set; }
        public double GDPTotal { get; set; }
        public double GDPInd { get; set; }
        public double GDPServ { get; set; }
        public double Inflation { get; set; }
        public String IndepDate { get; set; }
        public String Goverment { get; set; }
        public String CarCode { get; set; }
        public List<City> Cities { get; set; }
        public List<Ethnicgroups> Ethnicgroups_ { get; set; }
        public List<Religions> Religions_ { get; set; }
        public Encompassed Encompassed_ { get; set; }
        public List<Border> Borders { get; set; }
        public List<Province> Provinces { get; set; }
        public List<Languages> Languages_ { get; set; }
    }
}
