﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hazi3
{
    class Organization
    {
        public String ID { get; set; }
        public String Name { get; set; }
        public String Abbrev { get; set; }
        public String Established { get; set; }
        public String Headq { get; set; }
        public List<Members> M_embers { get; set; }
    }
}
