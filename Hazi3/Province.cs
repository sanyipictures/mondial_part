﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hazi3
{
    class Province
    {
        public String ID { get; set; }
        public String Name { get; set; }
        public String Country { get; set; }
        public String Capital { get; set; }
        public int Population_ { get; set; }
        public int Area { get; set; }
        public List<City> Cities { get; set; }
    }
}
